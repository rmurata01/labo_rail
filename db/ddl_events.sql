DROP TABLE IF EXISTS events;
CREATE TABLE events(
  id int primary key not null unique,
  events varchar ,
  contents text,
  square int,
  point int,
  rest int,
  place int
);