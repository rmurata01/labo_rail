DROP TABLE IF EXISTS questions;
CREATE TABLE questions(
  id int primary key not null unique,
  text varchar(200)
);
