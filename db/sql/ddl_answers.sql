DROP TABLE IF EXISTS answers;
CREATE TABLE answers(
  id int primary key not null unique,
  question_id int foreing key not null,
  text varchar(50),
  point integer(3),
  number integer(1),
  is_bounus boolean(1)
);
