DROP TABLE IF EXISTS users;
CREATE TABLE users(
  id integer primary key unique not null,
  user_name varchar not null,
  point int not null default 0,
  status varchar,
  place int not null default 0
);