#!/bin/sh
sqlite3 labo_rail.db < sql/dcl.sql
sqlite3 labo_rail.db < sql/ddl_users.sql
sqlite3 labo_rail.db < sql/ddl_events.sql
sqlite3 labo_rail.db < sql/ddl_questions.sql
sqlite3 labo_rail.db < sql/ddl_answers.sql
sqlite3 labo_rail.db < sql/dml_users.sql
sqlite3 labo_rail.db < sql/dml_events.sql
sqlite3 labo_rail.db < sql/dml_questions.sql
sqlite3 labo_rail.db < sql/dml_answers.sql



