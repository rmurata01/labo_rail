require "./dice.rb"
require "./user_frame"
require "./user"
require "./labo_rail_events.rb"
require "./rock_paper_scissors.rb"
require "./result.rb"
require "./quiz_event"

class LaboRailMap 
  def press_enter
    puts "\n Press Enter Key..."
    line = gets
  end
  def make_field
    @field = Array.new(3).map!{Array.new(10,nil)}
  end
  def setting_field
    make_field
    $users.each do | (k,user)|
      addr = calc_map_address_by(user.address)
      if (0..2).include?(addr[0]) && (0..9).include?(addr[1])
        @field[addr[0]][addr[1]] = user.marker
      end
    end
  end
  def calc_map_address_by(address)
    y = address/10
    if y.even? 
      x = address % 10 
    else 
      x = 9 - (address % 10)
    end
    return [y,x]
  end
  def print_field
    press_enter
    puts "\e[H\e[2J"
    @field.each do |row|
      puts "  +-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+"
      print "  |"
      row.each do |cell| c = cell.nil? ? " " : cell
        print "#{c.to_s.center(5)}|"
      end
      puts ""
    end
    puts "  +-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+"
  end
    
  def game_start     
    game_is_over = false
    until game_is_over do   
#    user_frame = UserFrame.new()
      $users.each do |key, user|
        #press_enter
        # puts "\e[H\e[2J"
        print_field
	puts "\n#{user.marker}#{user.user_name}さんの番です"
	if user.rest > 0
	  puts "残念！おやすみです！(press any key)"
	  gets
	  user.rest -= 1
	  next
	end
	puts "サイコロをふって下さい"
        dice = LaboRailDice.new()
        user.address += dice.start
        setting_field
        print_field 
	puts "\n#{user.marker}#{user.user_name}さんの番です"
	#ボーナスゲーム実施
	if user.address == 15
	  puts "*******************"
	  puts "   ボーナスゲーム"
	  puts "   じゃんけん"
	  puts "*******************"
	  sleep(1)
	  puts "じゃんけんに勝つと20ポイント獲得できます"
	  rps = Rock_paper_scissors.new
	  result = rps.start
	  if result
	    user.point += 20
	  end
	else
	  # イベント実施
          event = $events[user.address]
	  if event
	    event.show
	    user.point += event.point
	    user.rest += event.rest
	    user.address += event.square
	    user.address = 0 if user.address < 0
	    setting_field
	    #print_field
          elsif user.address < 29
            quiz = QuizEvent.new
            point = quiz.question
            user.point += point
	  end
        end
	#あるユーザーのゴール時
	if user.address >= 29
	   #press_enter
	   #puts "\e[H\e[2J"
	  puts " #{user.user_name}さん"
	  user.point += 30
	  game_is_over = true
	  break
	end
      end
    end
    result = Result.new()
    result.show
=begin   
    @field[][] = user_frame.frame1
    @field[][] = user_frame.frame2
    @field[][] = user_frame.frame3
    @field[][] = user_frame.frame4
=end
  end
end

if __FILE__ == $0
  user_frame = UserFrame.new()
  $users = {}
  u1 = User.new(2,"aaa",0,"",0, user_frame.frame1)
  u2 = User.new(3,"bbb",0,"",0,user_frame.frame2)
  u3 = User.new(4,"cccc",0,"",0,user_frame.frame3)
  u4 = User.new(5,"dddd",0,"",0,user_frame.frame4)
  #u1.address = 2
  #u2.address = 15
  #u3.address = 2
  #u4.address = 21
  $users[1] = u1
  $users[2] = u2
  $users[3] = u3
  $users[4] = u4
  require './load_execute' 
  load_execute = LoadExecute.new( 'db/labo_rail.db' )
  load_execute.run(3)
=begin
  $events = {}
  require 'rdbi'
  require 'rdbi-driver-sqlite3'
  dbh = RDBI.connect(:SQLite3, :database => 'db/labo_rail.db')
  dbh.execute("select * from events").each do |row|
    event = Event.new(row[0],row[1],row[2],row[3],row[4],row[5],row[6])
    $events[row[6]] = event
  end
=end
labo_rail_map = LaboRailMap.new()
=begin
p  labo_rail_map.calc_map_address_by(0)
p  labo_rail_map.calc_map_address_by(1)
p  labo_rail_map.calc_map_address_by(29)
p  labo_rail_map.calc_map_address_by(30)
p  labo_rail_map.calc_map_address_by(-1)
p  labo_rail_map.calc_map_address_by(-29)
p  labo_rail_map.calc_map_address_by(-30)
p  labo_rail_map.calc_map_address_by(-300)
=end
  labo_rail_map.make_field
  #labo_rail_map.print_field
  labo_rail_map.game_start
  #labo_rail_map.setting_field

  #labo_rail_map.print_field
end
