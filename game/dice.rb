class LaboRailDice
  def press_enter
    puts "\nPress Enter Key..."
    line = gets
  end
  def start
  press_enter
  dice = rand(6) + 1
    case dice
    when 1
      puts "+------+"
      puts "|  ・  |"
      puts "|      |"
      puts "+------+"
      puts "\n1進む"
    when 2
      puts "+------+"
      puts "| ・・ |"
      puts "|      |"
      puts "+------+"
      puts "\n2進む"
    when 3 
      puts "+------+"
      puts "|・・・|"
      puts "|      |"
      puts "+------+"
      puts "\n3進む"
    when 4
      puts "+------+"
      puts "| ・・ |"
      puts "| ・・ |"
      puts "+------+"
      puts "\n4進む"
    when 5 
      puts "+------+"
      puts "|・・・|"
      puts "| ・・ |"
      puts "+------+"
      puts "\n5進む"
    when 6 
      puts "+------+"
      puts "|・・・|"
      puts "|・・・|"
      puts "+------+"
      puts "\n6進む"
    end
    return dice
  end
end

if __FILE__ == $0
  labo_rail_dice = LaboRailDice.new()
  labo_rail_dice.start
end    
