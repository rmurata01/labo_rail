require 'rdbi'
require 'rdbi-driver-sqlite3'
require './user'
#require './dice'
require './user_frame'
#require './map'
#require './event_dice'
#require './rock_paper_scissors'
require './event'
#require './lab_rail_events'


class LoadExecute
  def initialize( db_file_name )
    @dbh = RDBI.connect( :SQLite3, :database => db_file_name )
  end

  def run(player_count)
    load_events
    load_questions
#    load_answers
    load_users(player_count)
     
  end

  private
  def load_users(player_count)
    $users = {}
    user_frame = UserFrame.new
    @dbh.execute( "select * from users order by id desc limit ?",player_count ).each_with_index do|row,i|
      id, user_name, point, status, place = row
      player_num = player_count-1-i
      marker = user_frame.get_frame_list[player_num]
      user = User.new( id, user_name, point, status, place, marker)
      $users[player_count-i] = user
    end
  end

  def load_events
    $events = {}
    @dbh.execute("select * from events").each do|row|
      id, events, contents, square, point, rest, place = row
      event = Event.new( id, events, contents, square, point, rest, place)
      $events[place] = event
    end
  end
  def load_questions
    $questions = []
    @dbh.execute("select * from questions").each do|row|
      id, text = row
      question = Question.new( id, text, fetch_answers_by(id))
      $questions.push(question)
    end
  end
  def fetch_answers_by(question_id)
    answers = {}
    results = @dbh.execute("select * from answers where question_id = ?", question_id)
    results.each do | row |
      id, question_id, text, point, number, is_bounus = row
      answer = Answer.new( id, question_id, text, point, number, is_bounus )
      answers[number] = answer 
    end
    return answers
  end
=begin
  def load_answers
    $answers = []
    @dbh.execute("select * from answers").each do|row|
      id, question_id, number, text, point, is_bounus = row
      answer = Answer.new( id, question_id, number, text, point, is_bounus )
      $answers.push(answer)
    end
  end
=end

end




class Question
  def initialize( id, text, answers )
    @id = id
    @text = text
    @answers = answers
  end
  attr_accessor :id, :text, :answers
end


class Answer
  def initialize( id, question_id, text, point, number, is_bounus )
    @id = id
    @question_id = question_id
    @text = text
    @point = point
    @number = number
    @is_bounus = is_bounus
  end
  attr_accessor :id, :question_id, :number, :text, :point, :is_bounus
end


if __FILE__ == $0
  load_execute = LoadExecute.new ("db/labo_rail.db")
  #question = Question.new("")
  #answer = Answer.new(0,0,"",0,0)
  #event = Event.new("","",0,0,0,0)
  load_execute.run
  #print "$events="
  #p $events
  #print "$questions="
  #p $questions

  puts "問題です"
  q = $questions[11]
  puts q.text
  q.answers.each do |a|
    puts "選択肢: #{a.number}"
    puts a.text
    puts "bounus? #{a.is_bounus}"
    puts "point: #{a.point}"
    
  end


end
