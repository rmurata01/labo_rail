require 'rdbi'
require 'rdbi-driver-sqlite3'
require 'date'
require './user'
require './load'
require './map'

class Title
  def initialize( db_file_name )
    @db_file_name = db_file_name 
    @dbh =  RDBI.connect( :SQLite3, :database => db_file_name )
  end
  def start
    system('clear')
    puts ""
    puts "間もなく電車が到着します。"
    puts "空いている扉よりご乗車下さい。(Enter)"
    line = gets.chomp
  end  

  def user_setting
    puts "何人で遊びますか？(2～4人まで)"
    print ">"
    @player_count = gets.chomp.to_i
    puts "#{@player_count}人で受付しました。"
    key = 0 

    @player_count.times{|i|
      key += 1
      user = User.new(0,"",0,"",0,"")
      print "#{key}人目のユーザーさん"
      puts "好きな名前を登録して下さい。" 
      print ">"
      user.user_name = gets.chomp.to_s
      @dbh.execute("insert into users(user_name) values (?)",
              user.user_name)
    }
    

   
  end

  system('clear')
  def run
    while true
      system('clear')
      print "

  __________          __  _  _ 
 |__________|        |  ||_||_|   
 ____________    ____|  |____
|_________   |  |____    ____| 
         /  /    __  |  |  __
        /  /    /  / |  | \\  \\
       /  /    /  /  |  |  \\  \\
      /__/    /__/   |__|   \\__\\  鉄

"
      puts "
1. start
9. end
番号を選んでください(1,9)"
      print ">"
      num = gets.chomp
      case num
      when '1'
        user_setting
        start
        load = Load.new()
        load.load_image(10, @db_file_name, @player_count)
        load.load_image_end
        labo_rail_map = LaboRailMap.new()
        labo_rail_map.make_field
        labo_rail_map.game_start
      when '9'
        puts "ご乗車ありがとうございました！！"
        puts "またのご利用をお待ちしております。"
        break;
      else
      end 
    end
  end
end


if __FILE__ == $0
  begin
    title = Title.new("../db/labo_rail.db")
    title.run
 # load = Load.new()    
  rescue Interrupt
    puts ""
    puts "ご乗車ありがとうございました！！"
    puts "またのご利用をお待ちしております。"
  end

#p $users
#puts $users[1].marker
#  $users.each do |k, user|
#   puts user.marker
#  end
#p $events
end
