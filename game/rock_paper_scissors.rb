class Rock_paper_scissors
  #キー押下を促すメッセージを表示し、
  #キー押下時に標準出力をクリアする
  def press_enter
    puts "\nPress Enter Key..."
    line = gets
  end
  #アプリケーション
  #def start
  def start
    result = false # 初期値：負け
    press_enter
    #プレイヤーのじゃんけん入力
    puts "何を出しますか？"
    puts "1:グー 2:チョキ 3:パー"
    input_value = gets
    #ロボットの処理
    robo = rand(3) + 1
    case robo
    when 1
      puts "ロボットはグーを出した！"
    when 2 
      puts "ロボットはチョキを出した！"
    when 3
      puts "ロボットはパーを出した！"
    end
    #じゃんけんの判定
    player = input_value.to_i
    case player
    when 1
      puts "あなたはグーを出した！"
      sleep 1
      case robo
      when 1 #ロボットがグーを出した場合
        puts "引き分けです(´･ω･`)"
        sleep 1
	start
      when 2 #ロボットがチョキを出した場合
        puts "あなたの勝ちです（＾o＾）☆"
	sleep(1)
	puts "20ポイント獲得！"
	result = true
      when 3 #ロボットがパーを出した場合
        puts "あなたの負けです（ToT）"
	result = false
      end
    when 2
      puts "あなたはチョキを出した！"
      sleep 1
      case robo
      when 1 #ロボットがグーを出した場合
        puts "あなたの負けです（ToT）"
	result = false
      when 2 #ロボットがチョキを出した場合
        puts "引き分けです(´･ω･`)"
	sleep 1
	start
      when 3 #ロボットがパーを出した場合
        puts "あなたの勝ちです（＾o＾）☆"
	sleep(1)
	puts "20ポイント獲得！"
	result = true
      end
    when 3
      puts "あなたはパーを出した！"
      sleep 1
      case robo
      when 1 #ロボットがグーを出した場合
        puts "あなたの勝ちです（＾o＾）☆"
	sleep(1)
	puts "20ポイント獲得！"
	result = true
      when 2 #ロボットがチョキを出した場合
        puts "あなたの負けです（ToT）"
	result = false
      when 3 #ロボットがパーを出した場合
        puts "引き分けです(´･ω･`)"
	sleep 1
	start
      end
    else #どの条件も成立しなかった場合
        sleep 1
        puts "あなたは何も出さなかった！"
	puts "あなたの負けです"
	result = false
    end
    sleep 1
    return result 
  end
end
if __FILE__ == $0
  rock_paper_scissors_game = Rock_paper_scissors.new()
  rock_paper_scissors_game.start
end
